# Version.Update.Hotfix (DD-MM-YYYY)

## 2.5.2 (26-02-2025)

- Now supports SRP rendering out of the box
- Small code cleanup and performance improvements

## 2.5.1 (23-11-2024)

- Fixed canvas warning showing even if animation editor has been collapsed
- Fixed additive calculation for the colors
- Fixed default value mode for the fill animations of the sprite bars
- Fixed ghost bar coloring in certain situations
- Updated tooltip for the value mode

## 2.5.0 (18-11-2024)

- **New** **Simple Mode**, allowing easier and faster bar creation.
- **New** tutorial video for using MicroBar in **Simple Mode**.
- Updated to **Unity 6** (still works on older versions).
- Added two new prefabs for Image and Sprite bars in Simple Mode.
- Added warning messages when sprite bars are added to the canvas.
- Added warning messages when image bars are added to the scene without a canvas parent.
- Fixed animation values for prefabs.
- Fixed positioning of background bars in Sprite Renderer mode; background bars now shake correctly.
- SpriteRenderer bars now use masks for fill amount instead of scale. Refer to the updated documentation under the “Render Type” and “NOT bar” sections.
- Improved visuals for SpriteRenderer bars. Backwards compatibility is maintained but warning messages are displayed.
- Scale is now an allowed command for SpriteRenderer bars and non-bars.
- Updated documentation.

## 2.4.1 (22-09-2024)

- Fixed RenderType enum type from internal to public

## 2.4.0 (04-09-2024)

- Support for the SpriteRenderer bars
- Added SpriteRenderer versions of the Image preset bars
- Image bars can now only spawn on canvases while Sprite bar on non-canvases
- Updated documentation

## 2.3.0 (29-08-2024)

- Added various tests to control the stability of the asset
- Implemented option to select formula for calculating HP after MaxHP change
- Current health can stay the same, follow the change amount of the max HP or keep it proportional to the old value

## 2.2.1 (23-07-2024)

- Added contact section to the README

## 2.2.0 (30-06-2024)

- Improvement: Removed glow effect for the editor performance
- Backend: Added integrated tests for the safer deployment
- Fixed: Z scale was set to 0f because of Vector2

## 2.1.2 (06-06-2024)

- Changed name of the "Basic" bar into "Simple" bar
- Setup of bar order in menu
- Added trailer video

## 2.1.1 (29-05-2024)

- Included MicroEditor scripts into the package

## 2.1.0 (24-05-2024)

- First asset store version

## 2.0.0 (20-05-2024)

- First complete version (MicroBar v2)
