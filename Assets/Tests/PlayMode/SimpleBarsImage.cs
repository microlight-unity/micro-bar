namespace Tests
{
#if UNITY_EDITOR
    using System.Collections;
    using NUnit.Framework;
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.TestTools;

    public class SimpleBarsImage
    {
        Canvas canvas;

        [SetUp]
        public void Setup()
        {
            canvas = Utility.EnsureCanvas();
        }

        ImageBarData GenerateImageBar(string path)
        {
            path = $"Assets/Tests/Prefabs/SimpleBarsImage/{path}.prefab";
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            Assert.NotNull(prefab, $"Prefab not found at path: {path}");
            GameObject instance = Object.Instantiate(prefab, canvas.transform);
            ImageBarData barData = Utility.CreateImageBarData(instance);
            barData.Bar.Initialize(150f);
            return barData;
        }

        [UnityTest]
        public IEnumerator NonAnimated()
        {
            ImageBarData barData = GenerateImageBar("NonAnimated");

            barData.Bar.UpdateBar(50f);
            Assert.AreEqual(150f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(50f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(0.333f).Within(Utility.TOLERANCE), "Health %");
            Assert.That(barData.BarImage.fillAmount, Is.EqualTo(0.333f).Within(Utility.TOLERANCE), "Bar fill amount");
            yield return null;
        }

        [UnityTest]
        public IEnumerator Color()
        {
            ImageBarData barData = GenerateImageBar("Color");

            barData.BarImage.color.Test(new Color(1f, 1f, 0f));
            yield return null;
        }

        [UnityTest]
        public IEnumerator AdaptiveColor()
        {
            ImageBarData barData = GenerateImageBar("AdaptiveColor");

            barData.Bar.UpdateBar(50f);
            barData.BarImage.color.Test(new Color(0.333f, 0.666f, 0.333f));
            yield return null;
        }

        [UnityTest]
        public IEnumerator GhostBar()
        {
            ImageBarData barData = GenerateImageBar("GhostBar");

            barData.GhostBarImage.color.Test(new Color(0f, 1f, 0.6f));
            barData.Bar.UpdateBar(50);
            barData.GhostBarImage.color.Test(new Color(0f, 1f, 0.6f));
            yield return null;
        }

        [UnityTest]
        public IEnumerator DualGhostBar()
        {
            ImageBarData barData = GenerateImageBar("DualGhostBar");

            barData.Bar.UpdateBar(50);
            barData.GhostBarImage.color.Test(new Color(1f, 0.3f, 0f));
            barData.Bar.UpdateBar(100);
            barData.GhostBarImage.color.Test(new Color(0f, 1f, 0.3f));
            yield return null;
        }

        [UnityTest]
        public IEnumerator DualGhostBarAnim()
        {
            ImageBarData barData = GenerateImageBar("DualGhostBarAnim");

            barData.Bar.UpdateBar(50);
            yield return new WaitForSeconds(0.2f);
            barData.GhostBarImage.color.Test(new Color(1f, 0.3f, 0f));

            barData.Bar.UpdateBar(100);
            yield return new WaitForSeconds(0.2f);
            barData.GhostBarImage.color.Test(new Color(0f, 1f, 0.3f));
            yield return null;
        }

        [UnityTest]
        public IEnumerator AnimFill()
        {
            ImageBarData barData = GenerateImageBar("AnimFill");

            barData.Bar.UpdateBar(50);

            Assert.That(barData.BarImage.fillAmount, Is.EqualTo(0.333f).Within(Utility.TOLERANCE), "Bar fill amount");
            Assert.That(barData.GhostBarImage.fillAmount, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Ghost Bar fill amount");

            yield return new WaitForSeconds(0.3f);
            Assert.That(barData.BarImage.fillAmount, Is.EqualTo(0.333f).Within(Utility.TOLERANCE), "Bar fill amount");
            Assert.That(barData.GhostBarImage.fillAmount, Is.EqualTo(0.333f).Within(Utility.TOLERANCE), "Ghost Bar fill amount");
        }

        [UnityTest]
        public IEnumerator AnimFlash()
        {
            ImageBarData barData = GenerateImageBar("AnimFlash");

            barData.Bar.UpdateBar(50);
            Assert.That(barData.BarImage.fillAmount, Is.EqualTo(0.333f).Within(Utility.TOLERANCE), "Bar fill amount");
            Assert.That(barData.GhostBarImage.fillAmount, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Ghost Bar fill amount");

            barData.BarImage.color.Test(UnityEngine.Color.white);
            barData.GhostBarImage.color.Test(UnityEngine.Color.white);

            yield return new WaitForSeconds(0.1f);
            barData.BarImage.color.Test(UnityEngine.Color.white);
            barData.GhostBarImage.color.Test(UnityEngine.Color.white);

            yield return new WaitForSeconds(0.4f);
            Assert.That(barData.BarImage.fillAmount, Is.EqualTo(0.333f).Within(Utility.TOLERANCE), "Bar fill amount");
            Assert.That(barData.GhostBarImage.fillAmount, Is.EqualTo(0.333f).Within(Utility.TOLERANCE), "Ghost Bar fill amount");
            barData.BarImage.color.Test(new Color(1f, 0.2f, 0.3f));
            barData.GhostBarImage.color.Test(UnityEngine.Color.white);
        }
    }
#endif
}
