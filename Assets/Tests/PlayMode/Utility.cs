// ****************************************************************************************************
// Various functions that help tests
// ****************************************************************************************************

namespace Tests
{
    using Microlight.MicroBar;
    using NUnit.Framework;
    using UnityEngine;
    using UnityEngine.UI;

    internal struct ImageBarData
    {
        internal MicroBar Bar;
        internal RectTransform Root;
        internal Image BackgroundImage;
        internal Image BarImage;
        internal Image GhostBarImage;
    }

    internal struct SpriteBarData
    {
        internal MicroBar Bar;
        internal Transform Root;
        internal SpriteRenderer BackgroundSprite;
        internal SpriteRenderer BarSprite;
        internal SpriteMask BarSpriteMask;
        internal SpriteRenderer GhostBarSprite;
        internal SpriteMask GhostBarSpriteMask;
    }

    internal static class Utility
    {
        internal const float TOLERANCE = 0.005f;

        /// <summary>
        /// Finds child with specified name in transform
        /// </summary>
        internal static Transform FindChildByName(Transform parent, string name)
        {
            foreach(Transform child in parent)
            {
                if(child.name == name)
                {
                    return child;
                }

                // Recursively search children
                Transform found = FindChildByName(child, name);
                if(found != null)
                {
                    return found;
                }
            }
            return null;
        }

        internal static Canvas EnsureCanvas()
        {
            Canvas canvas = Object.FindFirstObjectByType<Canvas>();
            if(canvas == null)
            {
                GameObject canvasGameObject = new GameObject("Canvas");
                canvas = canvasGameObject.AddComponent<Canvas>();
                canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            }
            Assert.NotNull(canvas, $"Canvas doesn't exist");
            return canvas;
        }

        internal static ImageBarData CreateImageBarData(GameObject instance)
        {
            ImageBarData data = new ImageBarData();
            data.Bar = instance.GetComponent<MicroBar>();
            Assert.NotNull(data.Bar, "MicroBar component not found");

            data.Root = instance.GetComponent<RectTransform>();
            Assert.NotNull(data.Root, "Root RectTransform not found");

            data.BackgroundImage = FindChildByName(data.Root, "HP Background").GetComponent<Image>();
            Assert.NotNull(data.BackgroundImage, "Background Image not found");

            data.BarImage = FindChildByName(data.Root, "HP Bar").GetComponent<Image>();
            Assert.NotNull(data.BarImage, "Bar Image not found");

            data.GhostBarImage = FindChildByName(data.Root, "HP Ghost Bar").GetComponent<Image>();
            Assert.NotNull(data.GhostBarImage, "Ghost Bar Image not found");
            return data;
        }

        internal static SpriteBarData CreateSpriteBarData(GameObject instance)
        {
            SpriteBarData data = new SpriteBarData();
            data.Bar = instance.GetComponent<MicroBar>();
            Assert.NotNull(data.Bar, "MicroBar component not found");

            data.Root = instance.GetComponent<Transform>();
            Assert.NotNull(data.Root, "Root Transform not found");

            data.BackgroundSprite = FindChildByName(data.Root, "HP Background").GetComponent<SpriteRenderer>();
            Assert.NotNull(data.BackgroundSprite, "Background SpriteRenderer not found");

            data.BarSprite = FindChildByName(data.Root, "HP Bar").GetComponent<SpriteRenderer>();
            Assert.NotNull(data.BarSprite, "Bar SpriteRenderer not found");

            data.BarSpriteMask = FindChildByName(data.Root, "HP Bar Mask").GetComponent<SpriteMask>();
            Assert.NotNull(data.BarSpriteMask, "Bar SpriteMask not found");

            data.GhostBarSprite = FindChildByName(data.Root, "HP Ghost Bar").GetComponent<SpriteRenderer>();
            Assert.NotNull(data.GhostBarSprite, "Ghost Bar SpriteRenderer not found");

            data.GhostBarSpriteMask = FindChildByName(data.Root, "HP Ghost Bar Mask").GetComponent<SpriteMask>();
            Assert.NotNull(data.BarSpriteMask, "Ghost Bar SpriteMask not found");

            return data;
        }

        internal static void Test(this Color color, Color expected, float tolerance = TOLERANCE)
        {
            Assert.That(color.r, Is.EqualTo(expected.r).Within(tolerance), "R");
            Assert.That(color.g, Is.EqualTo(expected.g).Within(tolerance), "G");
            Assert.That(color.b, Is.EqualTo(expected.b).Within(tolerance), "B");
            Assert.That(color.a, Is.EqualTo(expected.a).Within(tolerance), "A");
        }

        internal static void Test(this Vector3 vector, Vector3 expected, float tolerance = TOLERANCE)
        {
            Assert.That(vector.x, Is.EqualTo(expected.x).Within(tolerance), "X");
            Assert.That(vector.y, Is.EqualTo(expected.y).Within(tolerance), "Y");
            Assert.That(vector.z, Is.EqualTo(expected.z).Within(tolerance), "Z");
        }

        internal static void Test(this Vector2 vector, Vector2 expected, float tolerance = TOLERANCE)
        {
            Assert.That(vector.x, Is.EqualTo(expected.x).Within(tolerance), "X");
            Assert.That(vector.y, Is.EqualTo(expected.y).Within(tolerance), "Y");
        }
    }
}