namespace Tests
{
#if UNITY_EDITOR
    using System.Collections;
    using Microlight.MicroBar;
    using NUnit.Framework;
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.TestTools;

    public class AnimationCommandsImage
    {
        Canvas canvas;
        GraphicsDefaultValues startingBarValue;
        GraphicsDefaultValues startingGhostBarValue;

        [SetUp]
        public void Setup()
        {
            canvas = Utility.EnsureCanvas();
        }

        ImageBarData InitializeBar()
        {
            string prefabPath = "Assets/Tests/Prefabs/AnimationCommandsImageBar.prefab";
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath);
            Assert.NotNull(prefab, $"Prefab not found at path: {prefabPath}");
            GameObject instance = Object.Instantiate(prefab, canvas.transform);
            ImageBarData barData = Utility.CreateImageBarData(instance);
            startingBarValue = new GraphicsDefaultValues(barData.BarImage);
            startingGhostBarValue = new GraphicsDefaultValues(barData.GhostBarImage);
            barData.Bar.Initialize(150f);
            return barData;
        }

        #region Color

        // **************************************************
        // Color
        // **************************************************

        [UnityTest]
        public IEnumerator Color_Absolute()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);

            yield return new WaitForSeconds(0.2f);
            barData.BarImage.color.Test(new Color(0f, 1f, 0f, 1f));
        }

        [UnityTest]
        public IEnumerator Color_Additive()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);

            yield return new WaitForSeconds(0.2f);
            barData.BarImage.color.Test(new Color(1f, 0.2f, 1f, 1f));
        }

        [UnityTest]
        public IEnumerator Color_Multiplicative()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalDamage);

            yield return new WaitForSeconds(0.2f);
            barData.BarImage.color.Test(new Color(0f, 0.2f, 0f, 1f));
        }

        [UnityTest]
        public IEnumerator Color_Starting()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalHeal);

            yield return new WaitForSeconds(0.2f);
            barData.BarImage.color.Test(startingBarValue.Color);
        }

        [UnityTest]
        public IEnumerator Color_Default()
        {
            ImageBarData barData = InitializeBar();
            barData.BarImage.color = Color.blue;
            barData.Bar.SnapshotDefaultValues();

            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);
            yield return new WaitForSeconds(0.2f);
            barData.Bar.UpdateBar(75f, UpdateAnim.Armor);
            yield return new WaitForSeconds(0.2f);
            barData.BarImage.color.Test(Color.blue);
        }

        #endregion

        #region Fade

        // **************************************************
        // Fade
        // **************************************************

        [UnityTest]
        public IEnumerator Fade_Absolute()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.GhostBarImage.color.a, Is.EqualTo(0.8f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fade_Additive()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.GhostBarImage.color.a, Is.EqualTo(startingGhostBarValue.Fade - 0.25f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fade_Multiplicative()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalDamage);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.GhostBarImage.color.a, Is.EqualTo(startingGhostBarValue.Fade * 0.5f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fade_Starting()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalHeal);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.GhostBarImage.color.a, Is.EqualTo(startingGhostBarValue.Fade).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fade_Default()
        {
            ImageBarData barData = InitializeBar();
            barData.GhostBarImage.color = Color.blue;
            barData.Bar.SnapshotDefaultValues();

            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);
            yield return new WaitForSeconds(0.2f);
            barData.Bar.UpdateBar(75f, UpdateAnim.Armor);
            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.GhostBarImage.color.a, Is.EqualTo(1f).Within(Utility.TOLERANCE));
        }

        #endregion

        #region Fill

        // **************************************************
        // Fill
        // **************************************************

        [UnityTest]
        public IEnumerator Fill_NonCustom()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.DOT);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarImage.fillAmount, Is.EqualTo(barData.Bar.HPPercent).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fill_Absolute()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarImage.fillAmount, Is.EqualTo(0.2f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fill_Additive()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarImage.fillAmount, Is.EqualTo(startingBarValue.Fill - 0.3f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fill_Multiplicative()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(50f, UpdateAnim.CriticalDamage);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarImage.fillAmount, Is.EqualTo(startingBarValue.Fill * 0.25f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fill_Starting()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalHeal);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarImage.fillAmount, Is.EqualTo(startingBarValue.Fill).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fill_Default()
        {
            ImageBarData barData = InitializeBar();
            barData.BarImage.fillAmount = 0.3f;
            barData.Bar.SnapshotDefaultValues();

            barData.Bar.UpdateBar(75f, UpdateAnim.DOT);
            yield return new WaitForSeconds(0.2f);
            barData.Bar.UpdateBar(75f, UpdateAnim.Armor);
            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarImage.fillAmount, Is.EqualTo(0.3f).Within(Utility.TOLERANCE));
        }

        #endregion

        #region Move

        // **************************************************
        // Move
        // **************************************************

        [UnityTest]
        public IEnumerator Move_Absolute()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedPosition = new Vector3(5f, 2f, startingBarValue.Position.z);
            barData.BarImage.rectTransform.localPosition.Test(expectedPosition);
        }

        [UnityTest]
        public IEnumerator Move_Additive()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedPosition = new Vector3(startingBarValue.Position.x + 2f, startingBarValue.Position.y + 2f, startingBarValue.Position.z);
            barData.BarImage.rectTransform.localPosition.Test(expectedPosition);
        }

        [UnityTest]
        public IEnumerator Move_Multiplicative()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(50f, UpdateAnim.CriticalDamage);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedPosition = new Vector3(startingBarValue.Position.x * 3f, startingBarValue.Position.y, startingBarValue.Position.z);
            barData.BarImage.rectTransform.localPosition.Test(expectedPosition);
        }

        [UnityTest]
        public IEnumerator Move_Starting()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalHeal);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedPosition = new Vector3(startingBarValue.Position.x, startingBarValue.Position.y, startingBarValue.Position.z);
            barData.BarImage.rectTransform.localPosition.Test(expectedPosition);
        }

        [UnityTest]
        public IEnumerator Move_Default()
        {
            ImageBarData barData = InitializeBar();
            barData.BarImage.rectTransform.localPosition = new Vector3(1f, 1f, startingBarValue.Position.z);
            barData.Bar.SnapshotDefaultValues();

            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);
            yield return new WaitForSeconds(0.2f);
            barData.Bar.UpdateBar(75f, UpdateAnim.Armor);
            yield return new WaitForSeconds(0.2f);
            Vector3 expectedPosition = new Vector3(1f, 2f, startingBarValue.Position.z);
            barData.BarImage.rectTransform.localPosition.Test(expectedPosition);
        }

        #endregion

        #region Rotate

        // **************************************************
        // Rotate
        // **************************************************

        [UnityTest]
        public IEnumerator Rotate_Absolute()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarImage.rectTransform.localRotation.eulerAngles.z, Is.EqualTo(30f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Rotate_Additive()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarImage.rectTransform.localRotation.eulerAngles.z, Is.EqualTo(340f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Rotate_Multiplicative()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(50f, UpdateAnim.CriticalDamage);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarImage.rectTransform.localRotation.eulerAngles.z, Is.EqualTo(0f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Rotate_Starting()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalHeal);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarImage.rectTransform.localRotation.eulerAngles.z, Is.EqualTo(startingBarValue.Rotation).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Rotate_Default()
        {
            ImageBarData barData = InitializeBar();

            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);
            yield return new WaitForSeconds(0.2f);
            barData.Bar.UpdateBar(75f, UpdateAnim.Armor);
            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarImage.rectTransform.localRotation.eulerAngles.z, Is.EqualTo(startingBarValue.Rotation).Within(Utility.TOLERANCE));
        }

        #endregion

        #region Scale

        // **************************************************
        // Scale
        // **************************************************

        [UnityTest]
        public IEnumerator Scale_Absolute()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedScale = new Vector3(5f, 5f, startingBarValue.Scale.z);
            barData.BarImage.rectTransform.localScale.Test(expectedScale);
        }

        [UnityTest]
        public IEnumerator Scale_Additive()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedScale = new Vector3(startingBarValue.Scale.x - 0.2f, startingBarValue.Scale.y + 2f, startingBarValue.Scale.z);
            barData.BarImage.rectTransform.localScale.Test(expectedScale);
        }

        [UnityTest]
        public IEnumerator Scale_Multiplicative()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(50f, UpdateAnim.CriticalDamage);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedScale = new Vector3(startingBarValue.Scale.x * 0.4f, startingBarValue.Scale.y, startingBarValue.Scale.z);
            barData.BarImage.rectTransform.localScale.Test(expectedScale);
        }

        [UnityTest]
        public IEnumerator Scale_Starting()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalHeal);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedScale = new Vector3(startingBarValue.Scale.x, startingBarValue.Scale.y, startingBarValue.Scale.z);
            barData.BarImage.rectTransform.localScale.Test(expectedScale);
        }

        [UnityTest]
        public IEnumerator Scale_Default()
        {
            ImageBarData barData = InitializeBar();
            barData.BarImage.rectTransform.localScale = new Vector3(0.4f, 1.2f, startingBarValue.Scale.z);
            barData.Bar.SnapshotDefaultValues();

            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);
            yield return new WaitForSeconds(0.2f);
            barData.Bar.UpdateBar(75f, UpdateAnim.Armor);
            yield return new WaitForSeconds(0.2f);
            Vector3 expectedScale = new Vector3(5f, 1.2f, startingBarValue.Scale.z);
            barData.BarImage.rectTransform.localScale.Test(expectedScale);
        }

        #endregion

        #region Anchor move

        // **************************************************
        // Anchor move
        // **************************************************

        [UnityTest]
        public IEnumerator Anchor_Absolute()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);

            yield return new WaitForSeconds(0.2f);
            Vector2 expectedPosition = new Vector2(startingBarValue.AnchorPosition.x, 2f);
            barData.GhostBarImage.rectTransform.anchoredPosition.Test(expectedPosition);
        }

        [UnityTest]
        public IEnumerator Anchor_Additive()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);

            yield return new WaitForSeconds(0.2f);
            Vector2 expectedPosition = new Vector2(startingBarValue.AnchorPosition.x + 1.5f, startingBarValue.AnchorPosition.y);
            barData.GhostBarImage.rectTransform.anchoredPosition.Test(expectedPosition);
        }

        [UnityTest]
        public IEnumerator Anchor_Multiplicative()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(50f, UpdateAnim.CriticalDamage);

            yield return new WaitForSeconds(0.2f);
            Vector2 expectedPosition = new Vector2(startingBarValue.AnchorPosition.x * 2.2f, startingBarValue.AnchorPosition.y * 0.6f);
            barData.GhostBarImage.rectTransform.anchoredPosition.Test(expectedPosition);
        }

        [UnityTest]
        public IEnumerator Anchor_Starting()
        {
            ImageBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalHeal);

            yield return new WaitForSeconds(0.2f);
            Vector2 expectedPosition = new Vector2(startingBarValue.AnchorPosition.x, startingBarValue.AnchorPosition.y);
            barData.GhostBarImage.rectTransform.anchoredPosition.Test(expectedPosition);
        }

        [UnityTest]
        public IEnumerator Anchor_Default()
        {
            ImageBarData barData = InitializeBar();
            barData.GhostBarImage.rectTransform.anchoredPosition = new Vector2(0.8f, 1.6f);
            barData.Bar.SnapshotDefaultValues();

            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);
            yield return new WaitForSeconds(0.2f);
            barData.Bar.UpdateBar(75f, UpdateAnim.Armor);
            yield return new WaitForSeconds(0.2f);
            Vector2 expectedPosition = new Vector2(0.8f, 1.6f);
            barData.GhostBarImage.rectTransform.anchoredPosition.Test(expectedPosition);
        }

        #endregion
    }
#endif
}
