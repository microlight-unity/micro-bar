namespace Tests
{
#if UNITY_EDITOR
    using System.Collections;
    using NUnit.Framework;
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.TestTools;

    public class Initialization
    {
        Canvas canvas;

        [SetUp]
        public void Setup()
        {
            canvas = Utility.EnsureCanvas();
        }

        [UnityTest]
        public IEnumerator Image()
        {
            // Initialization
            string prefabPath = "Assets/Tests/Prefabs/MicroBarImage.prefab";
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath);
            Assert.NotNull(prefab, $"Prefab not found at path: {prefabPath}");
            GameObject instance = Object.Instantiate(prefab, canvas.transform);
            ImageBarData barData = Utility.CreateImageBarData(instance);
            barData.Bar.Initialize(150f);

            // Test
            Assert.AreEqual(150f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(150f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Health %");

            // Graphical
            Assert.That(barData.BarImage.fillAmount, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Bar fill amount");
            yield return null;
        }

        [UnityTest]
        public IEnumerator Sprite()
        {
            // Initialization
            string prefabPath = "Assets/Tests/Prefabs/MicroBarSprite.prefab";
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath);
            Assert.NotNull(prefab, $"Prefab not found at path: {prefabPath}");
            GameObject instance = Object.Instantiate(prefab);
            SpriteBarData barData = Utility.CreateSpriteBarData(instance);
            barData.Bar.Initialize(150f);

            // Test
            Assert.AreEqual(150f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(150f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Health %");

            // Graphical
            Assert.That(barData.BarSprite.transform.localScale.x, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Bar scale");
            Assert.That(barData.BarSpriteMask.transform.localScale.x, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Bar mask scale");

            Assert.That(barData.GhostBarSprite.transform.localScale.x, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Ghost Bar scale");
            Assert.That(barData.GhostBarSpriteMask.transform.localScale.x, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Ghost Bar mask scale");
            yield return null;
        }

        [UnityTest]
        public IEnumerator SimpleImage()
        {
            // Initialization
            string prefabPath = "Assets/Tests/Prefabs/SimpleBarImage.prefab";
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath);
            Assert.NotNull(prefab, $"Prefab not found at path: {prefabPath}");
            GameObject instance = Object.Instantiate(prefab, canvas.transform);
            ImageBarData barData = Utility.CreateImageBarData(instance);
            barData.Bar.Initialize(150f);

            // Test
            Assert.AreEqual(150f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(150f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Health %");

            // Graphical
            Assert.That(barData.BarImage.fillAmount, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Bar fill amount");
            yield return null;
        }

        [UnityTest]
        public IEnumerator SimpleSprite()
        {
            // Initialization
            string prefabPath = "Assets/Tests/Prefabs/SimpleBarSprite.prefab";
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath);
            Assert.NotNull(prefab, $"Prefab not found at path: {prefabPath}");
            GameObject instance = Object.Instantiate(prefab);
            SpriteBarData barData = Utility.CreateSpriteBarData(instance);
            barData.Bar.Initialize(150f);

            // Test
            Assert.AreEqual(150f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(150f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Health %");

            // Graphical
            Assert.That(barData.BarSprite.transform.localScale.x, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Bar scale");
            Assert.That(barData.BarSpriteMask.transform.localScale.x, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Bar mask scale");

            Assert.That(barData.GhostBarSprite.transform.localScale.x, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Ghost Bar scale");
            Assert.That(barData.GhostBarSpriteMask.transform.localScale.x, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Ghost Bar mask scale");
            yield return null;
        }
    }
#endif
}
