namespace Tests
{
#if UNITY_EDITOR
    using System.Collections;
    using NUnit.Framework;
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.TestTools;

    public class SimpleBarsSprite
    {
        SpriteBarData GenerateSpriteBar(string path)
        {
            path = $"Assets/Tests/Prefabs/SimpleBarsSprite/{path}.prefab";
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            Assert.NotNull(prefab, $"Prefab not found at path: {path}");
            GameObject instance = Object.Instantiate(prefab);
            SpriteBarData barData = Utility.CreateSpriteBarData(instance);
            barData.Bar.Initialize(150f);
            return barData;
        }

        [UnityTest]
        public IEnumerator NonAnimated()
        {
            SpriteBarData barData = GenerateSpriteBar("NonAnimated");

            barData.Bar.UpdateBar(50f);
            Assert.AreEqual(150f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(50f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(0.333f).Within(Utility.TOLERANCE), "Health %");
            barData.BarSprite.transform.localScale.Test(new Vector3(1f, 1f, 1f));
            barData.BarSpriteMask.transform.localScale.Test(new Vector3(0.333f, 1f, 1f));
            yield return null;
        }

        [UnityTest]
        public IEnumerator Color()
        {
            SpriteBarData barData = GenerateSpriteBar("Color");

            barData.BarSprite.color.Test(new Color(1f, 1f, 0f));
            yield return null;
        }

        [UnityTest]
        public IEnumerator AdaptiveColor()
        {
            SpriteBarData barData = GenerateSpriteBar("AdaptiveColor");

            barData.Bar.UpdateBar(50f);
            barData.BarSprite.color.Test(new Color(0.333f, 0.666f, 0.333f));
            yield return null;
        }

        [UnityTest]
        public IEnumerator GhostBar()
        {
            SpriteBarData barData = GenerateSpriteBar("GhostBar");

            barData.GhostBarSprite.color.Test(new Color(0f, 1f, 0.6f));
            barData.Bar.UpdateBar(50);
            barData.GhostBarSprite.color.Test(new Color(0f, 1f, 0.6f));
            yield return null;
        }

        [UnityTest]
        public IEnumerator DualGhostBar()
        {
            SpriteBarData barData = GenerateSpriteBar("DualGhostBar");

            barData.Bar.UpdateBar(50);
            barData.GhostBarSprite.color.Test(new Color(1f, 0.3f, 0f));
            barData.Bar.UpdateBar(100);
            barData.GhostBarSprite.color.Test(new Color(0f, 1f, 0.3f));
            yield return null;
        }

        [UnityTest]
        public IEnumerator DualGhostBarAnim()
        {
            SpriteBarData barData = GenerateSpriteBar("DualGhostBarAnim");

            barData.Bar.UpdateBar(50);
            yield return new WaitForSeconds(0.2f);
            barData.GhostBarSprite.color.Test(new Color(1f, 0.3f, 0f));

            barData.Bar.UpdateBar(100);
            yield return new WaitForSeconds(0.2f);
            barData.GhostBarSprite.color.Test(new Color(0f, 1f, 0.3f));
            yield return null;
        }

        [UnityTest]
        public IEnumerator AnimFill()
        {
            SpriteBarData barData = GenerateSpriteBar("AnimFill");

            barData.Bar.UpdateBar(50);

            barData.BarSprite.transform.localScale.Test(new Vector3(1f, 1f, 1f));
            barData.BarSpriteMask.transform.localScale.Test(new Vector3(0.333f, 1f, 1f));
            barData.GhostBarSprite.transform.localScale.Test(new Vector3(1f, 1f, 1f));
            barData.GhostBarSpriteMask.transform.localScale.Test(new Vector3(1f, 1f, 1f));

            yield return new WaitForSeconds(0.3f);
            barData.BarSprite.transform.localScale.Test(new Vector3(1f, 1f, 1f));
            barData.BarSpriteMask.transform.localScale.Test(new Vector3(0.333f, 1f, 1f));
            barData.GhostBarSprite.transform.localScale.Test(new Vector3(1f, 1f, 1f));
            barData.GhostBarSpriteMask.transform.localScale.Test(new Vector3(0.333f, 1f, 1f));
        }

        [UnityTest]
        public IEnumerator AnimFlash()
        {
            SpriteBarData barData = GenerateSpriteBar("AnimFlash");

            barData.Bar.UpdateBar(50);
            barData.BarSprite.transform.localScale.Test(new Vector3(1f, 1f, 1f));
            barData.BarSpriteMask.transform.localScale.Test(new Vector3(0.333f, 1f, 1f));
            barData.GhostBarSprite.transform.localScale.Test(new Vector3(1f, 1f, 1f));
            barData.GhostBarSpriteMask.transform.localScale.Test(new Vector3(1f, 1f, 1f));

            barData.BarSprite.color.Test(UnityEngine.Color.white);
            barData.GhostBarSprite.color.Test(UnityEngine.Color.white);

            yield return new WaitForSeconds(0.1f);
            barData.BarSprite.color.Test(UnityEngine.Color.white);
            barData.GhostBarSprite.color.Test(UnityEngine.Color.white);

            yield return new WaitForSeconds(0.4f);
            barData.BarSprite.transform.localScale.Test(new Vector3(1f, 1f, 1f));
            barData.BarSpriteMask.transform.localScale.Test(new Vector3(0.333f, 1f, 1f));
            barData.GhostBarSprite.transform.localScale.Test(new Vector3(1f, 1f, 1f));
            barData.GhostBarSpriteMask.transform.localScale.Test(new Vector3(0.333f, 1f, 1f));
            barData.BarSprite.color.Test(new Color(1f, 0.2f, 0.3f));
            barData.GhostBarSprite.color.Test(UnityEngine.Color.white);
        }
    }
#endif
}
