namespace Tests
{
#if UNITY_EDITOR
    using System.Collections;
    using Microlight.MicroBar;
    using NUnit.Framework;
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.TestTools;

    public class MaxHealth
    {
        SpriteBarData InitializeBar()
        {
            string prefabPath = "Assets/Tests/Prefabs/MicroBarSprite.prefab";
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath);
            Assert.NotNull(prefab, $"Prefab not found at path: {prefabPath}");
            GameObject instance = Object.Instantiate(prefab);
            SpriteBarData barData = Utility.CreateSpriteBarData(instance);
            barData.Bar.Initialize(150f);
            return barData;
        }

        #region Keep

        // **************************************************
        // Keep
        // **************************************************

        [UnityTest]
        public IEnumerator Keep_HigherMaxWhileFull()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Keep);
            barData.Bar.SetNewMaxHP(200f);
            yield return null;

            Assert.AreEqual(200f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(150f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(0.75f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator Keep_LowerMaxWhileFull()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Keep);
            barData.Bar.SetNewMaxHP(100f);
            yield return null;

            Assert.AreEqual(100f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(100f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator Keep_HigherMaxWhileHalf()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, true);
            yield return null;

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Keep);
            barData.Bar.SetNewMaxHP(200f);
            yield return null;

            Assert.AreEqual(200f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(75f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(0.375f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator Keep_LowerMaxButHigherThanCurrentWhileHalf()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, true);
            yield return null;

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Keep);
            barData.Bar.SetNewMaxHP(100f);
            yield return null;

            Assert.AreEqual(100f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(75f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(0.75f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator Keep_LowerMaxAndLowerThanCurrentWhileHalf()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, true);
            yield return null;

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Keep);
            barData.Bar.SetNewMaxHP(50f);
            yield return null;

            Assert.AreEqual(50f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(50f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Health %");
        }
        #endregion

        #region Follow

        // **************************************************
        // Follow
        // **************************************************

        [UnityTest]
        public IEnumerator Follow_HigherMaxWhileFull()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Follow);
            barData.Bar.SetNewMaxHP(200f);
            yield return null;

            Assert.AreEqual(200f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(200f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator Follow_LowerMaxWhileFull()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Follow);
            barData.Bar.SetNewMaxHP(100f);
            yield return null;

            Assert.AreEqual(100f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(100f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator Follow_HigherMaxWhileHalf()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, true);
            yield return null;

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Follow);
            barData.Bar.SetNewMaxHP(200f);
            yield return null;

            Assert.AreEqual(200f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(125f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(0.625f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator Follow_LowerMaxButHigherThanCurrentWhileHalf()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, true);
            yield return null;

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Follow);
            barData.Bar.SetNewMaxHP(100f);
            yield return null;

            Assert.AreEqual(100f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(25f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(0.25f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator Follow_LowerMaxAndLowerThanCurrentWhileHalf()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, true);
            yield return null;

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Follow);
            barData.Bar.SetNewMaxHP(50f);
            yield return null;

            Assert.AreEqual(50f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(1f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(0.02f).Within(Utility.TOLERANCE), "Health %");
        }
        #endregion

        #region Follow increase

        // **************************************************
        // Follow increase
        // **************************************************

        [UnityTest]
        public IEnumerator FollowIncrease_HigherMaxWhileFull()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.FollowIncrease);
            barData.Bar.SetNewMaxHP(200f);
            yield return null;

            Assert.AreEqual(200f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(200f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator FollowIncrease_LowerMaxWhileFull()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.FollowIncrease);
            barData.Bar.SetNewMaxHP(100f);
            yield return null;

            Assert.AreEqual(100f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(100f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator FollowIncrease_HigherMaxWhileHalf()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, true);
            yield return null;

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.FollowIncrease);
            barData.Bar.SetNewMaxHP(200f);
            yield return null;

            Assert.AreEqual(200f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(125f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(0.625f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator FollowIncrease_LowerMaxButHigherThanCurrentWhileHalf()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, true);
            yield return null;

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.FollowIncrease);
            barData.Bar.SetNewMaxHP(100f);
            yield return null;

            Assert.AreEqual(100f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(75f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(0.75f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator FollowIncrease_LowerMaxAndLowerThanCurrentWhileHalf()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, true);
            yield return null;

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.FollowIncrease);
            barData.Bar.SetNewMaxHP(50f);
            yield return null;

            Assert.AreEqual(50f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(50f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Health %");
        }
        #endregion

        #region Proportional

        // **************************************************
        // Proportional
        // **************************************************

        [UnityTest]
        public IEnumerator Proportional_HigherMaxWhileFull()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Proportional);
            barData.Bar.SetNewMaxHP(200f);
            yield return null;

            Assert.AreEqual(200f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(200f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator Proportional_LowerMaxWhileFull()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Proportional);
            barData.Bar.SetNewMaxHP(100f);
            yield return null;

            Assert.AreEqual(100f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(100f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator Proportional_HigherMaxWhileHalf()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, true);
            yield return null;

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Proportional);
            barData.Bar.SetNewMaxHP(200f);
            yield return null;

            Assert.AreEqual(200f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(100f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(0.5f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator Proportional_LowerMaxButHigherThanCurrentWhileHalf()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, true);
            yield return null;

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Proportional);
            barData.Bar.SetNewMaxHP(100f);
            yield return null;

            Assert.AreEqual(100f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(50f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(0.5f).Within(Utility.TOLERANCE), "Health %");
        }

        [UnityTest]
        public IEnumerator Proportional_LowerMaxAndLowerThanCurrentWhileHalf()
        {
            // Initialize
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, true);
            yield return null;

            // Test
            MicroBar.ChangeMaxHealthCalculation(MaxHealthCalculation.Proportional);
            barData.Bar.SetNewMaxHP(50f);
            yield return null;

            Assert.AreEqual(50f, barData.Bar.MaxValue, "Max health");
            Assert.AreEqual(25f, barData.Bar.CurrentValue, "Current health");
            Assert.That(barData.Bar.HPPercent, Is.EqualTo(0.5f).Within(Utility.TOLERANCE), "Health %");
        }
        #endregion
    }
#endif
}
