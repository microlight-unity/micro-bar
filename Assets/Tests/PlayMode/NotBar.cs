namespace Tests
{
#if UNITY_EDITOR
    using System.Collections;
    using NUnit.Framework;
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.TestTools;

    public class NotBar
    {
        Canvas canvas;

        [SetUp]
        public void Setup()
        {
            canvas = Utility.EnsureCanvas();
        }

        [UnityTest]
        public IEnumerator Sprite()
        {
            // Initialization
            string prefabPath = "Assets/Tests/Prefabs/NotBarSpriteTestBar.prefab";
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath);
            Assert.NotNull(prefab, $"Prefab not found at path: {prefabPath}");
            GameObject instance = Object.Instantiate(prefab);
            SpriteBarData barData = Utility.CreateSpriteBarData(instance);
            barData.Bar.Initialize(150f);

            // Test
            barData.Bar.UpdateBar(75f, true);
            yield return null;

            Assert.That(barData.BarSprite.transform.localScale.x, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Bar scale");
            Assert.That(barData.BarSpriteMask.transform.localScale.x, Is.EqualTo(0.5f).Within(Utility.TOLERANCE), "Bar mask scale");

            Assert.That(barData.GhostBarSprite.transform.localScale.x, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Ghost Bar scale");
            Assert.That(barData.GhostBarSpriteMask.transform.localScale.x, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Ghost Bar mask scale");
            yield return null;
        }

        [UnityTest]
        public IEnumerator Image()
        {
            // Initialization
            string prefabPath = "Assets/Tests/Prefabs/NotBarImageTestBar.prefab";
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath);
            Assert.NotNull(prefab, $"Prefab not found at path: {prefabPath}");
            GameObject instance = Object.Instantiate(prefab);
            ImageBarData barData = Utility.CreateImageBarData(instance);
            barData.Bar.Initialize(150f);

            // Test
            barData.Bar.UpdateBar(75f, true);
            yield return null;

            Assert.That(barData.BarImage.fillAmount, Is.EqualTo(0.5f).Within(Utility.TOLERANCE), "Bar fill amount");
            Assert.That(barData.GhostBarImage.fillAmount, Is.EqualTo(1f).Within(Utility.TOLERANCE), "Ghost Bar fill amount");
            yield return null;
        }
    }
#endif
}
