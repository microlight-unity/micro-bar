namespace Tests
{
#if UNITY_EDITOR
    using System.Collections;
    using Microlight.MicroBar;
    using NUnit.Framework;
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.TestTools;

    public class AnimationCommandsSprite
    {
        GraphicsDefaultValues startingBarValue;
        Vector3 startingBarMaskScale;
        GraphicsDefaultValues startingGhostBarValue;
        Vector3 startingGhostBarMaskScale;

        SpriteBarData InitializeBar()
        {
            string prefabPath = "Assets/Tests/Prefabs/AnimationCommandsSpriteBar.prefab";
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath);
            Assert.NotNull(prefab, $"Prefab not found at path: {prefabPath}");
            GameObject instance = Object.Instantiate(prefab);
            SpriteBarData barData = Utility.CreateSpriteBarData(instance);
            startingBarValue = new GraphicsDefaultValues(barData.BarSprite);
            startingBarMaskScale = barData.BarSpriteMask.transform.localScale;
            startingGhostBarValue = new GraphicsDefaultValues(barData.GhostBarSprite);
            startingGhostBarMaskScale = barData.BarSpriteMask.transform.localScale;
            barData.Bar.Initialize(150f);
            return barData;
        }

        #region Color

        // **************************************************
        // Color
        // **************************************************

        [UnityTest]
        public IEnumerator Color_Absolute()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);

            yield return new WaitForSeconds(0.2f);
            barData.BarSprite.color.Test(new Color(0f, 1f, 0f, 1f));
        }

        [UnityTest]
        public IEnumerator Color_Additive()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);

            yield return new WaitForSeconds(0.2f);
            barData.BarSprite.color.Test(new Color(1f, 0.2f, 1f, 1f));
        }

        [UnityTest]
        public IEnumerator Color_Multiplicative()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalDamage);

            yield return new WaitForSeconds(0.2f);
            barData.BarSprite.color.Test(new Color(0f, 0.2f, 0f, 1f));
        }

        [UnityTest]
        public IEnumerator Color_Starting()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalHeal);

            yield return new WaitForSeconds(0.2f);
            barData.BarSprite.color.Test(startingBarValue.Color);
        }

        [UnityTest]
        public IEnumerator Color_Default()
        {
            SpriteBarData barData = InitializeBar();
            barData.BarSprite.color = Color.blue;
            barData.Bar.SnapshotDefaultValues();

            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);
            yield return new WaitForSeconds(0.2f);
            barData.Bar.UpdateBar(75f, UpdateAnim.Armor);
            yield return new WaitForSeconds(0.2f);
            barData.BarSprite.color.Test(Color.blue);
        }

        #endregion

        #region Fade

        // **************************************************
        // Fade
        // **************************************************

        [UnityTest]
        public IEnumerator Fade_Absolute()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.GhostBarSprite.color.a, Is.EqualTo(0.8f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fade_Additive()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.GhostBarSprite.color.a, Is.EqualTo(startingGhostBarValue.Fade - 0.25f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fade_Multiplicative()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalDamage);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.GhostBarSprite.color.a, Is.EqualTo(startingGhostBarValue.Fade * 0.5f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fade_Starting()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalHeal);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.GhostBarSprite.color.a, Is.EqualTo(startingGhostBarValue.Fade).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fade_Default()
        {
            SpriteBarData barData = InitializeBar();
            barData.GhostBarSprite.color = Color.blue;
            barData.Bar.SnapshotDefaultValues();

            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);
            yield return new WaitForSeconds(0.2f);
            barData.Bar.UpdateBar(75f, UpdateAnim.Armor);
            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.GhostBarSprite.color.a, Is.EqualTo(1f).Within(Utility.TOLERANCE));
        }

        #endregion

        #region Fill

        // **************************************************
        // Fill
        // **************************************************

        [UnityTest]
        public IEnumerator Fill_NonCustom()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.DOT);

            yield return new WaitForSeconds(0.5f);
            barData.GhostBarSprite.transform.localScale.Test(startingGhostBarValue.Scale);
            Assert.That(barData.GhostBarSpriteMask.transform.localScale.x, Is.EqualTo(barData.Bar.HPPercent).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fill_Absolute()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);

            yield return new WaitForSeconds(0.2f);
            barData.GhostBarSprite.transform.localScale.Test(startingGhostBarValue.Scale);
            Assert.That(barData.GhostBarSpriteMask.transform.localScale.x, Is.EqualTo(0.2f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fill_Additive()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);

            yield return new WaitForSeconds(0.2f);
            barData.GhostBarSprite.transform.localScale.Test(startingGhostBarValue.Scale);
            Assert.That(barData.GhostBarSpriteMask.transform.localScale.x, Is.EqualTo(startingGhostBarMaskScale.x - 0.3f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fill_Multiplicative()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(50f, UpdateAnim.CriticalDamage);

            yield return new WaitForSeconds(0.2f);
            barData.GhostBarSprite.transform.localScale.Test(startingGhostBarValue.Scale);
            Assert.That(barData.GhostBarSpriteMask.transform.localScale.x, Is.EqualTo(startingGhostBarMaskScale.x * 0.25f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fill_Starting()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalHeal);

            yield return new WaitForSeconds(0.2f);
            barData.GhostBarSprite.transform.localScale.Test(startingGhostBarValue.Scale);
            Assert.That(barData.GhostBarSpriteMask.transform.localScale.x, Is.EqualTo(startingGhostBarMaskScale.x).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Fill_Default()
        {
            SpriteBarData barData = InitializeBar();
            barData.GhostBarSpriteMask.transform.localScale = new Vector3(0.3f, barData.GhostBarSpriteMask.transform.localScale.y, barData.GhostBarSpriteMask.transform.localScale.z);
            barData.Bar.SnapshotDefaultValues();

            barData.Bar.UpdateBar(75f, UpdateAnim.DOT);
            yield return new WaitForSeconds(0.2f);
            barData.Bar.UpdateBar(75f, UpdateAnim.Armor);
            yield return new WaitForSeconds(0.2f);
            barData.GhostBarSprite.transform.localScale.Test(startingGhostBarValue.Scale);
            Assert.That(barData.GhostBarSpriteMask.transform.localScale.x, Is.EqualTo(0.3f).Within(Utility.TOLERANCE));
        }

        #endregion

        #region Move

        // **************************************************
        // Move
        // **************************************************

        [UnityTest]
        public IEnumerator Move_Absolute()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedPosition = new Vector3(5f, 2f, startingBarValue.Position.z);
            barData.BarSprite.transform.localPosition.Test(expectedPosition);
        }

        [UnityTest]
        public IEnumerator Move_Additive()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedPosition = new Vector3(startingBarValue.Position.x + 2f, startingBarValue.Position.y + 2f, startingBarValue.Position.z);
            barData.BarSprite.transform.localPosition.Test(expectedPosition);
        }

        [UnityTest]
        public IEnumerator Move_Multiplicative()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(50f, UpdateAnim.CriticalDamage);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedPosition = new Vector3(startingBarValue.Position.x * 3f, startingBarValue.Position.y, startingBarValue.Position.z);
            barData.BarSprite.transform.localPosition.Test(expectedPosition);
        }

        [UnityTest]
        public IEnumerator Move_Starting()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalHeal);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedPosition = new Vector3(startingBarValue.Position.x, startingBarValue.Position.y, startingBarValue.Position.z);
            barData.BarSprite.transform.localPosition.Test(expectedPosition);
        }

        [UnityTest]
        public IEnumerator Move_Default()
        {
            SpriteBarData barData = InitializeBar();
            barData.BarSprite.transform.localPosition = new Vector3(1f, 1f, startingBarValue.Position.z);
            barData.Bar.SnapshotDefaultValues();

            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);
            yield return new WaitForSeconds(0.2f);
            barData.Bar.UpdateBar(75f, UpdateAnim.Armor);
            yield return new WaitForSeconds(0.2f);
            Vector3 expectedPosition = new Vector3(1f, 2f, startingBarValue.Position.z);
            barData.BarSprite.transform.localPosition.Test(expectedPosition);
        }

        #endregion

        #region Rotate

        // **************************************************
        // Rotate
        // **************************************************

        [UnityTest]
        public IEnumerator Rotate_Absolute()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarSprite.transform.localRotation.eulerAngles.z, Is.EqualTo(30f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Rotate_Additive()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarSprite.transform.localRotation.eulerAngles.z, Is.EqualTo(340f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Rotate_Multiplicative()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(50f, UpdateAnim.CriticalDamage);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarSprite.transform.localRotation.eulerAngles.z, Is.EqualTo(0f).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Rotate_Starting()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalHeal);

            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarSprite.transform.localRotation.eulerAngles.z, Is.EqualTo(startingBarValue.Rotation).Within(Utility.TOLERANCE));
        }

        [UnityTest]
        public IEnumerator Rotate_Default()
        {
            SpriteBarData barData = InitializeBar();

            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);
            yield return new WaitForSeconds(0.2f);
            barData.Bar.UpdateBar(75f, UpdateAnim.Armor);
            yield return new WaitForSeconds(0.2f);
            Assert.That(barData.BarSprite.transform.localRotation.eulerAngles.z, Is.EqualTo(startingBarValue.Rotation).Within(Utility.TOLERANCE));
        }

        #endregion

        #region Scale

        // **************************************************
        // Scale
        // **************************************************

        [UnityTest]
        public IEnumerator Scale_Absolute()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedScale = new Vector3(5f, 5f, startingBarValue.Scale.z);
            barData.BarSprite.transform.localScale.Test(expectedScale);
            barData.BarSpriteMask.transform.localScale.Test(startingBarMaskScale);
        }

        [UnityTest]
        public IEnumerator Scale_Additive()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.Heal);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedScale = new Vector3(startingBarValue.Scale.x - 0.2f, startingBarValue.Scale.y + 2f, startingBarValue.Scale.z);
            barData.BarSprite.transform.localScale.Test(expectedScale);
            barData.BarSpriteMask.transform.localScale.Test(startingBarMaskScale);
        }

        [UnityTest]
        public IEnumerator Scale_Multiplicative()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(50f, UpdateAnim.CriticalDamage);

            Debug.Log(barData.BarSpriteMask.transform.localScale);
            yield return new WaitForSeconds(0.2f);
            Debug.Log(barData.BarSpriteMask.transform.localScale);
            Vector3 expectedScale = new Vector3(startingBarValue.Scale.x * 0.4f, startingBarValue.Scale.y, startingBarValue.Scale.z);
            barData.BarSprite.transform.localScale.Test(expectedScale);
            barData.BarSpriteMask.transform.localScale.Test(startingBarMaskScale);
        }

        [UnityTest]
        public IEnumerator Scale_Starting()
        {
            SpriteBarData barData = InitializeBar();
            barData.Bar.UpdateBar(75f, UpdateAnim.CriticalHeal);

            yield return new WaitForSeconds(0.2f);
            Vector3 expectedScale = new Vector3(startingBarValue.Scale.x, startingBarValue.Scale.y, startingBarValue.Scale.z);
            barData.BarSprite.transform.localScale.Test(expectedScale);
            barData.BarSpriteMask.transform.localScale.Test(startingBarMaskScale);
        }

        [UnityTest]
        public IEnumerator Scale_Default()
        {
            SpriteBarData barData = InitializeBar();
            barData.BarSprite.transform.localScale = new Vector3(0.4f, 1.2f, startingBarValue.Scale.z);
            barData.Bar.SnapshotDefaultValues();

            barData.Bar.UpdateBar(75f, UpdateAnim.Damage);
            yield return new WaitForSeconds(0.2f);
            barData.Bar.UpdateBar(75f, UpdateAnim.Armor);
            yield return new WaitForSeconds(0.2f);
            Vector3 expectedScale = new Vector3(5f, 1.2f, startingBarValue.Scale.z);
            barData.BarSprite.transform.localScale.Test(expectedScale);
            barData.BarSpriteMask.transform.localScale.Test(startingBarMaskScale);
        }

        #endregion
    }
#endif
}
