HOW ARE TESTS DONE?

1. Initialize bar and test starting values
2. Prepare bar with values (usually instant update rather than animated) and test if its updated properly
3. Update bar with animation
4. Test the starting value (some values are updated instantly while others are animated)
5. Test during the animation. 
    - This should be done with caution because even it easily goes out of sync
    - Worst is when animation is long and changes same parameter in two animation events
6. Test end state if everything has finished correctly.
    - This test should be performed a bit after it finished, to give it time if something is out of sync