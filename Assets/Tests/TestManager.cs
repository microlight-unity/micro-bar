using UnityEngine;

namespace Microlight.MicroBar
{
    public class TestManager : MonoBehaviour
    {
        [Header("Image Bars")]
        [SerializeField] MicroBar uiSimple;
        [SerializeField] MicroBar uiDelayed;
        [SerializeField] MicroBar uiDisappear;
        [SerializeField] MicroBar uiImpact;
        [SerializeField] MicroBar uiPunch;
        [SerializeField] MicroBar uiShake;

        [Header("Sprite Bars")]
        [SerializeField] MicroBar srSimple;
        [SerializeField] MicroBar srDelayed;
        [SerializeField] MicroBar srDisappear;
        [SerializeField] MicroBar srImpact;
        [SerializeField] MicroBar srPunch;
        [SerializeField] MicroBar srShake;

        [Header("Simple Bars")]
        [SerializeField] MicroBar uiSimpleBar;
        [SerializeField] MicroBar srSimpleBar;

        const float MAX_HP = 100f;

        private void Start()
        {
            uiSimple.Initialize(MAX_HP);
            uiDelayed.Initialize(MAX_HP);
            uiDisappear.Initialize(MAX_HP);
            uiImpact.Initialize(MAX_HP);
            uiPunch.Initialize(MAX_HP);
            uiShake.Initialize(MAX_HP);

            srSimple.Initialize(MAX_HP);
            srDelayed.Initialize(MAX_HP);
            srDisappear.Initialize(MAX_HP);
            srImpact.Initialize(MAX_HP);
            srPunch.Initialize(MAX_HP);
            srShake.Initialize(MAX_HP);

            uiSimpleBar.Initialize(MAX_HP);
            srSimpleBar.Initialize(MAX_HP);
        }

        public void Damage()
        {
            float damageAmount = Random.Range(5f, 15f);
            float newValue = uiSimple.CurrentValue - damageAmount;

            uiSimple.UpdateBar(newValue);
            uiDelayed.UpdateBar(newValue);
            uiDisappear.UpdateBar(newValue);
            uiImpact.UpdateBar(newValue);
            uiPunch.UpdateBar(newValue);
            uiShake.UpdateBar(newValue);

            srSimple.UpdateBar(newValue);
            srDelayed.UpdateBar(newValue);
            srDisappear.UpdateBar(newValue);
            srImpact.UpdateBar(newValue);
            srPunch.UpdateBar(newValue);
            srShake.UpdateBar(newValue);

            uiSimpleBar.UpdateBar(newValue);
            srSimpleBar.UpdateBar(newValue);
        }

        public void Heal()
        {
            float healAmount = Random.Range(5f, 15f);
            float newValue = uiSimple.CurrentValue + healAmount;

            uiSimple.UpdateBar(newValue, UpdateAnim.Heal);
            uiDelayed.UpdateBar(newValue, UpdateAnim.Heal);
            uiDisappear.UpdateBar(newValue, UpdateAnim.Heal);
            uiImpact.UpdateBar(newValue, UpdateAnim.Heal);
            uiPunch.UpdateBar(newValue, UpdateAnim.Heal);
            uiShake.UpdateBar(newValue, UpdateAnim.Heal);

            srSimple.UpdateBar(newValue, UpdateAnim.Heal);
            srDelayed.UpdateBar(newValue, UpdateAnim.Heal);
            srDisappear.UpdateBar(newValue, UpdateAnim.Heal);
            srImpact.UpdateBar(newValue, UpdateAnim.Heal);
            srPunch.UpdateBar(newValue, UpdateAnim.Heal);
            srShake.UpdateBar(newValue, UpdateAnim.Heal);

            uiSimpleBar.UpdateBar(newValue);
            srSimpleBar.UpdateBar(newValue);
        }
    }
}