using Microlight.MicroBar;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    [SerializeField] MicroBar healthBar;
    [SerializeField] MicroBar staminaBar;

    private void Start()
    {
        healthBar.Initialize(100f);
        staminaBar.Initialize(50f);
    }

    public void Damage()
    {
        float damageAmount = Random.Range(5f, 15f);
        healthBar.UpdateBar(healthBar.CurrentValue - damageAmount);
        staminaBar.UpdateBar(staminaBar.CurrentValue - damageAmount);
    }

    public void Heal()
    {
        float healAmount = Random.Range(5f, 15f);
        healthBar.UpdateBar(healthBar.CurrentValue + healAmount);
        staminaBar.UpdateBar(staminaBar.CurrentValue + healAmount);
    }
}